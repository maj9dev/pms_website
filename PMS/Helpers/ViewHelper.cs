﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PMS.Helpers
{
    public class ViewHelper
    {
        public RouteValueDictionary ConditionalDisable(bool isDetail, object htmlAttributes = null)
        {
            var dictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (isDetail)
            {
                dictionary.Add("disabled", "disabled");
            }

            return dictionary;
        }
    }
}