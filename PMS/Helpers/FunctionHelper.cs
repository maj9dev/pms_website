﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Helpers
{
    public class FunctionHelper
    {
        public string TimeAgo(DateTime dateTime)
        {
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(dateTime);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("เมื่อ {0} วินาทีก่อน", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("เมื่อ {0} นาทีก่อน", timeSpan.Minutes) :
                    "ประมาณนาทีที่ผ่านมา";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("เมื่อ {0} ชั่วโมงก่อน", timeSpan.Hours) :
                    "ประมาณชั่วโมงที่ผ่านมา";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("เมื่อ {0} วันก่อน", timeSpan.Days) :
                    "ประมาณหนึ่งวันผ่านมา";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("เมื่อ {0} เดือนก่อน", timeSpan.Days / 30) :
                    "ประมาณหนึ่งเดือนที่ผ่านมา";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("เมื่อ {0} ปีก่อน", timeSpan.Days / 365) :
                    "ประมาณหนึ่งปีผ่านมา";
            }

            return result;
        }

    }
}