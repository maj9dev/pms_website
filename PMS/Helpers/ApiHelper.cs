﻿using Newtonsoft.Json;
using PMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace PMS.Helpers
{
    public class ApiHelper
    {
        private readonly string baseUrl = WebConfigurationManager.AppSettings["ApiUrl"].ToString();
        private readonly string project = "projects";
        private readonly string projectType = "projectTypes";
        private readonly string projectStatus = "projectstatus";
        private readonly string member = "members";
        
        #region Project
        public async Task<ProjectModel> GetProject(int id)
        {
            var result = new ApiResponse<ProjectModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + project + "/" + id);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<ProjectModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<List<ProjectModel>> GetAllProject()
        {
            var result = new ApiResponse<List<ProjectModel>>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + project + "/getall");

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<List<ProjectModel>>>(response);
                }
            }

            return result.Data;
        }

        public async Task<ProjectModel> CreateProject(ProjectModel data)
        {
            var result = new ApiResponse<ProjectModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string json = JsonConvert.SerializeObject(data, Formatting.Indented);

                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.PostAsync(project, httpContent);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<ProjectModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<ProjectModel> UpdateProject(ProjectModel data)
        {
            var result = new ApiResponse<ProjectModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string json = JsonConvert.SerializeObject(data, Formatting.Indented);

                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.PutAsync(project + "/" + data.Id, httpContent);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<ProjectModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<bool> DeleteProject(int id)
        {
            var result = new ApiResponse<bool>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.DeleteAsync(project + "/" + id);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<bool>>(response);
                }
            }

            return result.Data;
        }
        #endregion

        #region Project Type
        public async Task<ProjectModel> GetProjectType(int id)
        {
            var result = new ApiResponse<ProjectModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + projectType + "/" + id);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<ProjectModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<List<ProjectModel>> GetAllProjectType()
        {
            var result = new ApiResponse<List<ProjectModel>>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + projectType + "/getall");

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<List<ProjectModel>>>(response);
                }
            }

            return result.Data;
        }
        #endregion

        #region Project Status
        public async Task<ProjectModel> GetProjectStatus(int id)
        {
            var result = new ApiResponse<ProjectModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + projectStatus + "/" + id);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<ProjectModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<List<ProjectModel>> GetAllProjectStatus()
        {
            var result = new ApiResponse<List<ProjectModel>>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + projectStatus + "/getall");

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<List<ProjectModel>>>(response);
                }
            }

            return result.Data;
        }
        #endregion

        #region Member
        public async Task<MemberModel> GetMember(int id)
        {
            var result = new ApiResponse<MemberModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + member + "/" + id);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<MemberModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<List<MemberModel>> GetAllMember()
        {
            var result = new ApiResponse<List<MemberModel>>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/" + member + "/getall");

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<List<MemberModel>>>(response);
                }
            }

            return result.Data;
        }

        public async Task<MemberModel> CreateMember(MemberModel data)
        {
            var result = new ApiResponse<MemberModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string json = JsonConvert.SerializeObject(data, Formatting.Indented);

                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.PostAsync(member, httpContent);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<MemberModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<MemberModel> UpdateMember(MemberModel data)
        {
            var result = new ApiResponse<MemberModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string json = JsonConvert.SerializeObject(data, Formatting.Indented);

                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.PutAsync(member + "/" + data.Id, httpContent);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<MemberModel>>(response);
                }
            }

            return result.Data;
        }

        public async Task<bool> DeleteMember(int id)
        {
            var result = new ApiResponse<bool>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.DeleteAsync(member + "/" + id);

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<bool>>(response);
                }
            }

            return result.Data;
        }
        #endregion


    }
}