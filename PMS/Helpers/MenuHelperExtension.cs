﻿using PMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Helpers
{
    public class MenuHelperExtension
    {

        public Menu GetListMenu(bool active = true)
        {
            Menu menu = new Menu();
            menu.items = new List<MenuItem>();

            if (active)
            {
                // Mockup
                menu.items.Add(new MenuItem { Id = 1, ParentId = null, NameTH = "หน้าหลัก", NameEn = "Dashboard", ControllerName = "Home", ActionName = "Index", CssClass = "fa fa-bar-chart-o", Role = "", Active = true });
                menu.items.Add(new MenuItem { Id = 2, ParentId = null, NameTH = "ยื่นเสนอหัวข้อโครงการ", NameEn = "Registration Proposal", ControllerName = "Proposal", ActionName = "Registration", CssClass = "fa fa-edit", Role = "", Active = true });
                menu.items.Add(new MenuItem { Id = 3, ParentId = null, NameTH = "โครงการ", NameEn = "Project", ControllerName = "Project", ActionName = "Report", CssClass = "fa fa-book", Role = "", Active = true });
                menu.items.Add(new MenuItem { Id = 4, ParentId = null, NameTH = "ประเมินโครงการ", NameEn = "Assessment", ControllerName = "Assessment", ActionName = "Index", CssClass = "fa fa-tasks", Role = "", Active = true });
                menu.items.Add(new MenuItem { Id = 5, ParentId = null, NameTH = "จัดการผู้ใช้งาน", NameEn = "User Management", ControllerName = "User", ActionName = "Index", CssClass = "fa fa-users", Role = "", Active = true });
                menu.items.Add(new MenuItem { Id = 6, ParentId = null, NameTH = "ตั้งค่า", NameEn = "Setting", ControllerName = "Setting", ActionName = "Index", CssClass = "fa fa-cog", Role = "", Active = true });

                // Get List Menu
            }

            return menu;
        }

    }
}