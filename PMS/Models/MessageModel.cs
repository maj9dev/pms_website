﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class MessageModel
    {
        public int Id { get; set; }

        public string CoverImg { get; set; }

        public string Message { get; set; }

        public string Owner { get; set; }

        public string EarlierTime { get; set; }

        public DateTime PostDate { get; set; }

    }
}