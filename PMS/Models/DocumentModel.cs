﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class DocumentModel
    {
        public int Id { get; set; }

        public string DocName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}