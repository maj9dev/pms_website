﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class AssessmentProjectModel
    {
        public int Id { get; set; }
        public string WeekName { get; set; }
        public string WorkDiscription { get; set; }
        public double Point { get; set; }
        public int Status { get; set; }

        public List<QuestionAssessmentModel> ListQuestionAsset { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}