﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class QuestionAssessmentModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public int AssessmentId { get; set; }
        public bool IsAssesment { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreateBy { get; set; }
    }
}