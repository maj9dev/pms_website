﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class ProjectModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "nameTh")]
        public string NameTh { get; set; }
        [JsonProperty(PropertyName = "nameEn")]
        public string NameEn { get; set; }
        [JsonProperty(PropertyName = "annual")]
        public int? Annual { get; set; }
        [JsonProperty(PropertyName = "Abstract")]
        public string Abstract { get; set; }
        [JsonProperty(PropertyName = "purpose")]
        public string Purpose { get; set; }
        [JsonProperty(PropertyName = "statusId")]
        public int StatusId { get; set; }
        [JsonProperty(PropertyName = "typeId")]
        public int TypeId { get; set; }
        [JsonProperty(PropertyName = "approveById")]
        public int? ApproveById { get; set; }
        [JsonProperty(PropertyName = "approveDate")]
        public DateTime? ApproveDate { get; set; }
        [JsonProperty(PropertyName = "createdById")]
        public int? CreatedById { get; set; }
        [JsonProperty(PropertyName = "createdDate")]
        public DateTime? CreatedDate { get; set; }
        [JsonProperty(PropertyName = "modifiedById")]
        public int? ModifiedById { get; set; }
        [JsonProperty(PropertyName = "modifiedDate")]
        public DateTime? ModifiedDate { get; set; }
        [JsonProperty(PropertyName = "projectMember")]
        public ICollection<ProjectMemberModel> ProjectMember { get; set; }

    }
}