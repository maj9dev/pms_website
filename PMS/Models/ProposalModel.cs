﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class ProposalModel
    {
        [Required]
        [JsonProperty(PropertyName = "subject")]
        public string Subject { get; set; }

        [Required]
        [JsonProperty(PropertyName = "propose")]
        public string Propose { get; set; }


        // Other
        [Required]
        public string[] Members { get; set; }

        [Required]
        public string[] Teachers { get; set; }

    }
}