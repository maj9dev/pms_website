﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class MemberModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "userId")]
        public int? UserId { get; set; }
        [JsonProperty(PropertyName = "memberTypeId")]
        public int MemberTypeId { get; set; }
        [JsonProperty(PropertyName = "no")]
        public string No { get; set; }
        [JsonProperty(PropertyName = "titleId")]
        public int TitleId { get; set; }
        [JsonProperty(PropertyName = "firstname")]
        public string Firstname { get; set; }
        [JsonProperty(PropertyName = "lastname")]
        public string Lastname { get; set; }
        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }
        [JsonProperty(PropertyName = "dateOfBirth")]
        public DateTime? DateOfBirth { get; set; }
        [JsonProperty(PropertyName = "coverImageId")]
        public int? CoverImageId { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "phoneNumber")]
        public string PhoneNumber { get; set; }
        [JsonProperty(PropertyName = "isActive")]
        public bool? IsActive { get; set; }
        [JsonProperty(PropertyName = "createdById")]
        public int? CreatedById { get; set; }
        [JsonProperty(PropertyName = "createdDate")]
        public DateTime? CreatedDate { get; set; }
        [JsonProperty(PropertyName = "modifiedById")]
        public int? ModifiedById { get; set; }
        [JsonProperty(PropertyName = "modifiedDate")]
        public DateTime? ModifiedDate { get; set; }
        [JsonProperty(PropertyName = "memberType")]
        public MemberTypeModel MemberType { get; set; }
    }
}