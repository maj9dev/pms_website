﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class CalendarModel
    {
        public int Id { get; set; }

        public string Title { get; set;}

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool AllDay { get; set; }

        public string Url { get; set; }
    }
}