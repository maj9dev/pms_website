﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class Menu
    {
        public List<MenuItem> items { get; set; }
    }

    public class MenuItem
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string NameTH { get; set; }
        public string NameEn { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string CssClass { get; set; }
        public string Role { get; set; }
        public bool? Active { get; set; }
    }

}