﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Models
{
    public class ProjectMemberModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "projectId")]
        public int ProjectId { get; set; }
        [JsonProperty(PropertyName = "memberId")]
        public int MemberId { get; set; }
        [JsonProperty(PropertyName = "createdDate")]
        public DateTime? CreatedDate { get; set; }
    }
}