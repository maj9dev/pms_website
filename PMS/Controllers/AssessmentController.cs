﻿using PMS.Helpers;
using PMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace PMS.Controllers
{
    public class AssessmentController : Controller
    {
        ApiHelper api = new ApiHelper();

        // GET: Assessment
        public ActionResult Index()
        {
            ViewBag.Title = "ประเมินโครงการ";
            return View();
        }

        public ActionResult AssessmentProject(int? projectId)
        {
            return View();
        }

        public ActionResult _ListAssessmentProject(int? projectId)
        {
            List<AssessmentProjectModel> list = new List<AssessmentProjectModel>();

            for (int i = 0; i < 9; i++)
            {
                list.Add(new AssessmentProjectModel { Id = i + 1, WeekName = "สัปดาห์ที่ " + (i + 1), WorkDiscription = "ประเมินผลเสร็จสิ้น", Point = 20, CreatedDate = DateTime.Now, Status = 1 });
            }

            for (int i = list.Count; i < 16; i++)
            {
                list.Add(new AssessmentProjectModel { Id = i + 1, WeekName = "สัปดาห์ที่ " + (i + 1), WorkDiscription = "รอการประเมินผล", Point = 0, CreatedDate = DateTime.Now, Status = 2 });
            }

            ViewBag.Title = "การประเมินโครงการ";
            ViewBag.Project = "ทดสอบหมายเลขที่ 1";

            return PartialView(list);
        }

        public ActionResult _TblAssessmentPartial()
        {
            List<ProjectModel> list = new List<ProjectModel>();

            // trick to prevent deadlocks of calling async method 
            // and waiting for on a sync UI thread.
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);

            //  this is the async call, wait for the result (!)
            list = api.GetAllProject().Result;

            // restore the context
            SynchronizationContext.SetSynchronizationContext(syncContext);

            return PartialView(list);
        }

        public ActionResult _ModalAssessmentPartial(int? Id)
        {
            AssessmentProjectModel model = new AssessmentProjectModel();

            model.Id = 1;
            model.WeekName = "สัปดาห์ที่ 1";
            model.WorkDiscription = "ประเมินผลเสร็จสิ้น";
            model.Status = 1;
            model.Point = 20;
            model.CreatedDate = DateTime.Now;

            List<QuestionAssessmentModel> list = new List<QuestionAssessmentModel>();

            for (int i = 0; i < 3; i++)
            {
                list.Add(new QuestionAssessmentModel { Id = i + 1, AssessmentId = Id.Value, Question = "หัวข้อประเมินโครงการ " + i, IsAssesment = true, CreatedDate = DateTime.Now });
            }

            for (int i = list.Count; i < 6; i++)
            {
                list.Add(new QuestionAssessmentModel { Id = i + 1, AssessmentId = Id.Value, Question = "หัวข้อประเมินโครงการ " + i, IsAssesment = false, CreatedDate = DateTime.Now });
            }

            model.ListQuestionAsset = list;

            return PartialView(model);
        }
    }
}