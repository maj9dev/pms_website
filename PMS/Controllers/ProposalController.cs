﻿using Newtonsoft.Json;
using PMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace PMS.Controllers
{
    public class ProposalController : Controller
    {
        private readonly string baseUrl = WebConfigurationManager.AppSettings["ApiUrl"].ToString();

        // GET: ProposeTopic
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration()
        {
            ViewBag.Title = "ยื่นเสนอหัวข้อโครงการ";
            return View();
        }

        public ActionResult Upload(int id)
        {
            ProjectModel data = new ProjectModel();

            data.Id = id;
            data.Code = "P64000001";
            data.NameTh = "โครงการทดสอบหมายเลขที่ 1";

            data.StatusId = 1;

            data.CreatedDate = DateTime.Now;

            ViewBag.Title = "อัพโหลดเอกสารโครงการ";

            return View(data);
        }

        public ActionResult Result(int id)
        {
            ProjectModel data = new ProjectModel();

            data.Id = id;
            data.Code = "P64000001";
            data.NameTh = "โครงการทดสอบหมายเลขที่ 1";

            data.StatusId = 1;

            data.CreatedDate = DateTime.Now;

            ViewBag.Title = "ผลการลงทะเบียน";

            return View(data);
        }


        #region Function
        private async Task<List<ProjectModel>> GetAllProposal()
        {
            var result = new ApiResponse<List<ProjectModel>>();
            List<ProjectModel> _list = new List<ProjectModel>();

            using (var client = new HttpClient())
            {
                // Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();
                // Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage res = await client.GetAsync("api/project/getall");

                // Checking the response is successful or not which is sent using HttpClient  
                if (res.IsSuccessStatusCode)
                {
                    // Storing the response details recieved from web api   
                    var response = res.Content.ReadAsStringAsync().Result;

                    // Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<ApiResponse<List<ProjectModel>>>(response);
                }
            }

            return result.Data;
        }

        [HttpPost]
        public async Task<ActionResult> Save(ProposalModel data)
        {
            string message = "";

            try
            {
                if (data != null)
                {
                    message = "successful";
                }
                else
                {
                    message = "Upload file error : Can not save, please check your project id and your file.";
                }
            }
            catch (Exception ex)
            {
                message = "Upload file error : " + ex.Message;
            }

            // TODO Save proposal record

            await Task.Delay(1000);

            return Json(new { Data = 1999, Message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Upload(FileUploadModel data)
        {
            string message = "";

            try
            {
                if (data != null)
                {
                    byte[] fileInBytes = ConvertToByte(data.Files);

                    data.FileType = data.Files.ContentType;
                    data.FileName = data.Files.FileName;
                    data.FileLength = data.Files.ContentLength;

                    if (fileInBytes != null)
                    {
                        var fileBase64String = Convert.ToBase64String(fileInBytes);

                        // TODO Save file record

                        await Task.Delay(1000);

                        message = "successful";
                    }
                }
                else
                {
                    message = "Upload file error : Can not save, please check your project id and your file.";
                }
            }
            catch (Exception ex)
            {
                message = "Upload file error : " + ex.Message;
            }

            return Json(new { Data = 1999, Message = message }, JsonRequestBehavior.AllowGet);
        }

        private byte[] ConvertToByte(HttpPostedFileBase data)
        {
            if (data != null)
            {
                byte[] fileInBytes = new byte[data.ContentLength];
                BinaryReader rdr = new BinaryReader(data.InputStream);
                fileInBytes = rdr.ReadBytes(data.ContentLength);

                rdr.Close();

                return fileInBytes;
            }

            return null;
        }
        #endregion


    }
}