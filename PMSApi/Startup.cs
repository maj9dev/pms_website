using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PMSApi.Interfaces;
using PMSApi.Models;
using PMSApi.Services;

namespace PMSApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Include NewtonsoftJson
            services.AddControllers().AddNewtonsoftJson();

            // Include Interface
            services.AddScoped<IMemberService, MemberService>();
            services.AddScoped<IMemberTypeService, MemberTypeService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IProjectStatusService, ProjectStatusService>();
            services.AddScoped<IProjectTypeService, ProjectTypeService>();

            // Include Auto Mapper
            services.AddAutoMapper(typeof(Startup));

            // Add Cache
            services.AddResponseCaching();

            // Add Config Service
            services.AddDbContext<PMSContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
