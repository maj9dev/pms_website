﻿using Microsoft.AspNetCore.Mvc;
using PMSApi.Interfaces;
using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PMSApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _services;

        public ProjectsController(IProjectService services)
        {
            this._services = services;
        }

        // GET: api/<MemberController>
        [HttpGet]
        [Route("getall")]
        public async Task<IActionResult> GetAll()
        {
            var response  = await _services.GetList();

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // GET api/<MemberController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var response = await _services.GetById(id);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // POST api/<MemberController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProjectDto model)
        {
            var response = await _services.Create(model);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // PUT api/<MemberController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ProjectDto model)
        {
            model.Id = id;

            var response = await _services.Update(model);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // DELETE api/<MemberController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _services.GetById(id);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }
    }
}
