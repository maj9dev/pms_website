﻿using Microsoft.AspNetCore.Mvc;
using PMSApi.Interfaces;
using PMSApi.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PMSApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MembersController : ControllerBase
    {
        private readonly IMemberService _service;

        public MembersController(IMemberService service)
        {
            this._service = service;
        }

        // GET: api/<MemberController>
        [HttpGet]
        [Route("getall")]
        public async Task<IActionResult> GetAll()
        {
            var response = await _service.GetList();

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // GET api/<MemberController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var response = await _service.GetById(id);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // POST api/<MemberController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MemberDto model)
        {
            var response = await _service.Create(model);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // PUT api/<MemberController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] MemberDto model)
        {
            model.Id = id;

            var response = await _service.Update(model);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        // DELETE api/<MemberController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _service.Delete(id);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }
    }
}
