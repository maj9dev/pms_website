﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Dtos
{
    public class MemberDto
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int MemberTypeId { get; set; }
        public string No { get; set; }
        public int TitleId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? CoverImageId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool? IsActive { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedById { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public MemberTypeDto MemberType { get; set; }
    }
}
