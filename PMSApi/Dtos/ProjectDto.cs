﻿using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Dtos
{
    public class ProjectDto
    {
        
        public int Id { get; set; }
        public string Code { get; set; }
        public string NameTh { get; set; }
        public string NameEn { get; set; }
        public int? Annual { get; set; }
        public string Abstract { get; set; }
        public string Purpose { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public int? ApproveById { get; set; }
        public DateTime? ApproveDate { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedById { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public ICollection<ProjectMemberDto> ProjectMember { get; set; }
    }
}
