﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMSApi.Interfaces;
using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Services
{
    public class ProjectService : IProjectService
    {
        private readonly PMSContext _context;
        private readonly IMapper _mapper;

        // Constructor
        public ProjectService(PMSContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<ApiResponse<ProjectDto>> Create(ProjectDto data)
        {
            var response = new ApiResponse<ProjectDto>();

            try
            {
                var project = _mapper.Map<Project>(data);

                await _context.Project.AddAsync(project);

                foreach (var item in data.ProjectMember)
                {
                    ProjectMember member = new ProjectMember();

                    member.ProjectId = data.Id;
                    member.MemberId = item.Id;

                    _context.ProjectMember.Add(member);
                }

                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<ProjectDto>(project);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<ProjectDto>> Update(ProjectDto data)
        {
            var response = new ApiResponse<ProjectDto>();

            try
            {
                var project = _mapper.Map<Project>(data);

                _context.Project.Update(project);

                // Check member have change ??
                if (true)
                {
                    _context.ProjectMember.RemoveRange(_context.ProjectMember.Where(w => w.Id == project.Id));
                }

                await _context.SaveChangesAsync();
                response.Data = data;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<bool>> Delete(int id)
        {
            var response = new ApiResponse<bool>();
            response.Data = false;
            response.IsSuccess = false;

            try
            {
                Project data = await _context.Project.Where(w => w.Id.Equals(id)).FirstOrDefaultAsync();
                _context.Project.Remove(data);
                await _context.SaveChangesAsync();

                response.Data = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<ProjectDto>> GetById(int id)
        {
            var response = new ApiResponse<ProjectDto>();

            try
            {
                var data = await _context.Project.Where(w => w.Id.Equals(id)).Include(i => i.ProjectMember).FirstOrDefaultAsync();

                response.Data = _mapper.Map<ProjectDto>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<List<ProjectDto>>> GetList()
        {
            var response = new ApiResponse<List<ProjectDto>>();

            try
            {
                var list = await _context.Project.ToListAsync();

                response.Data = _mapper.Map<List<ProjectDto>>(list);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

    }
}
