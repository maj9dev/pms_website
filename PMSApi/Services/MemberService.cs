﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMSApi.Interfaces;
using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Services
{
    public class MemberService : IMemberService
    {
        private readonly PMSContext _context;
        private readonly IMapper _mapper;

        public MemberService(PMSContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<ApiResponse<MemberDto>> Create(MemberDto data)
        {
            var response = new ApiResponse<MemberDto>();

            try
            {
                var member = _mapper.Map<Member>(data);

                _context.Member.Add(member);

                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<MemberDto>(member);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<MemberDto>> Update(MemberDto data)
        {
            var response = new ApiResponse<MemberDto>();

            try
            {
                var member = _mapper.Map<Member>(data);

                _context.Member.Update(member);
                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<MemberDto>(member);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<bool>> Delete(int id)
        {
            var response = new ApiResponse<bool>();
            response.Data = false;
            response.IsSuccess = false;

            try
            {
                Member data = _context.Member.Where(w => w.Id == id).FirstOrDefault();

                _context.Member.Remove(data);

                await _context.SaveChangesAsync();

                response.Data = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<MemberDto>> GetById(int id)
        {
            var response = new ApiResponse<MemberDto>();

            try
            {
                var data = await _context.Member.Where(w => w.Id.Equals(id)).Include(i => i.MemberType).FirstOrDefaultAsync();

                response.Data = _mapper.Map<MemberDto>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<List<MemberDto>>> GetList()
        {
            var response = new ApiResponse<List<MemberDto>>();

            try
            {
                var data = await _context.Member.Include(i => i.MemberType).ToListAsync();

                response.Data = _mapper.Map<List<MemberDto>>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

    }
}
