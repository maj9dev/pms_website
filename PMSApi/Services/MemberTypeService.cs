﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMSApi.Interfaces;
using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Services
{
    public class MemberTypeService : IMemberTypeService
    {
        private readonly PMSContext _context;
        private readonly IMapper _mapper;

        public MemberTypeService(PMSContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<ApiResponse<MemberTypeDto>> Create(MemberTypeDto data)
        {
            var response = new ApiResponse<MemberTypeDto>();

            try
            {
                var map = _mapper.Map<MemberType>(data);

                _context.MemberType.Add(map);
                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<MemberTypeDto>(map);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<MemberTypeDto>> Update(MemberTypeDto data)
        {
            var response = new ApiResponse<MemberTypeDto>();

            try
            {
                var map = _mapper.Map<MemberType>(data);

                _context.MemberType.Update(map);
                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<MemberTypeDto>(map);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<bool>> Delete(int id)
        {
            var response = new ApiResponse<bool>();
            response.Data = false;
            response.IsSuccess = false;

            try
            {
                MemberType data = _context.MemberType.Where(w => w.Id == id).FirstOrDefault();

                _context.MemberType.Remove(data);
                await _context.SaveChangesAsync();

                response.Data = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<MemberTypeDto>> GetById(int id)
        {
            var response = new ApiResponse<MemberTypeDto>();

            try
            {
                var data = await _context.MemberType.Where(w => w.Id.Equals(id)).FirstOrDefaultAsync();

                response.Data = _mapper.Map<MemberTypeDto>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<List<MemberTypeDto>>> GetList()
        {
            var response = new ApiResponse<List<MemberTypeDto>>();

            try
            {
                var data = await _context.MemberType.ToListAsync();

                response.Data = _mapper.Map<List<MemberTypeDto>>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

    }
}
