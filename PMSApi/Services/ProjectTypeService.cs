﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMSApi.Interfaces;
using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Services
{
    public class ProjectTypeService : IProjectTypeService
    {
        private readonly PMSContext _context;
        private readonly IMapper _mapper;

        public ProjectTypeService(PMSContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<ApiResponse<ProjectTypeDto>> Create(ProjectTypeDto data)
        {
            var response = new ApiResponse<ProjectTypeDto>();

            try
            {
                var map = _mapper.Map<ProjectType>(data);

                _context.ProjectType.Add(map);
                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<ProjectTypeDto>(map);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<ProjectTypeDto>> Update(ProjectTypeDto data)
        {
            var response = new ApiResponse<ProjectTypeDto>();

            try
            {
                var map = _mapper.Map<ProjectType>(data);

                _context.ProjectType.Update(map);
                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<ProjectTypeDto>(map);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<bool>> Delete(int id)
        {
            var response = new ApiResponse<bool>();
            response.Data = false;
            response.IsSuccess = false;

            try
            {
                ProjectType data = _context.ProjectType.Where(w => w.Id == id).FirstOrDefault();

                _context.ProjectType.Remove(data);
                await _context.SaveChangesAsync();

                response.Data = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<ProjectTypeDto>> GetById(int id)
        {
            var response = new ApiResponse<ProjectTypeDto>();

            try
            {
                var data = await _context.ProjectType.Where(w => w.Id.Equals(id)).FirstOrDefaultAsync();

                response.Data = _mapper.Map<ProjectTypeDto>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<List<ProjectTypeDto>>> GetList()
        {
            var response = new ApiResponse<List<ProjectTypeDto>>();

            try
            {
                var data = await _context.ProjectType.ToListAsync();

                response.Data = _mapper.Map<List<ProjectTypeDto>>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

    }
}
