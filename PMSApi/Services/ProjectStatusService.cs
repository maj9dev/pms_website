﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMSApi.Interfaces;
using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Services
{
    public class ProjectStatusService : IProjectStatusService
    {
        private readonly PMSContext _context;
        private readonly IMapper _mapper;

        public ProjectStatusService(PMSContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<ApiResponse<ProjectStatusDto>> Create(ProjectStatusDto data)
        {
            var response = new ApiResponse<ProjectStatusDto>();

            try
            {
                var map = _mapper.Map<ProjectStatus>(data);

                _context.ProjectStatus.Add(map);
                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<ProjectStatusDto>(map);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<ProjectStatusDto>> Update(ProjectStatusDto data)
        {
            var response = new ApiResponse<ProjectStatusDto>();

            try
            {
                var map = _mapper.Map<ProjectStatus>(data);

                _context.ProjectStatus.Update(map);
                await _context.SaveChangesAsync();

                response.Data = _mapper.Map<ProjectStatusDto>(map);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<bool>> Delete(int id)
        {
            var response = new ApiResponse<bool>();
            response.Data = false;
            response.IsSuccess = false;

            try
            {
                ProjectStatus data = _context.ProjectStatus.Where(w => w.Id == id).FirstOrDefault();

                _context.ProjectStatus.Remove(data);
                await _context.SaveChangesAsync();

                response.Data = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }
            
            return response;
        }

        public async Task<ApiResponse<ProjectStatusDto>> GetById(int id)
        {
            var response = new ApiResponse<ProjectStatusDto>();

            try
            {
                var data = await _context.ProjectStatus.Where(w => w.Id.Equals(id)).FirstOrDefaultAsync();

                response.Data = _mapper.Map<ProjectStatusDto>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task<ApiResponse<List<ProjectStatusDto>>> GetList()
        {
            var response = new ApiResponse<List<ProjectStatusDto>>();

            try
            {
                var data = await _context.ProjectStatus.ToListAsync();

                response.Data = _mapper.Map<List<ProjectStatusDto>>(data);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

    }
}
