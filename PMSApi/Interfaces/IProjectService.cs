﻿using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi.Interfaces
{
    public interface IProjectService
    {
        Task<ApiResponse<ProjectDto>> Create(ProjectDto data);
        Task<ApiResponse<ProjectDto>> Update(ProjectDto data);
        Task<ApiResponse<bool>> Delete(int id);
        Task<ApiResponse<ProjectDto>> GetById(int id);
        Task<ApiResponse<List<ProjectDto>>> GetList();
    }
}
