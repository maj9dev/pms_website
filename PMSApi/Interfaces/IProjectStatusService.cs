﻿using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSApi.Interfaces
{
    public interface IProjectStatusService
    {
        Task<ApiResponse<ProjectStatusDto>> Create(ProjectStatusDto data);
        Task<ApiResponse<ProjectStatusDto>> Update(ProjectStatusDto data);
        Task<ApiResponse<bool>> Delete(int id);
        Task<ApiResponse<ProjectStatusDto>> GetById(int id);
        Task<ApiResponse<List<ProjectStatusDto>>> GetList();
    }
}
