﻿using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSApi.Interfaces
{
    public interface IProjectTypeService
    {
        Task<ApiResponse<ProjectTypeDto>> Create(ProjectTypeDto data);
        Task<ApiResponse<ProjectTypeDto>> Update(ProjectTypeDto data);
        Task<ApiResponse<bool>> Delete(int id);
        Task<ApiResponse<ProjectTypeDto>> GetById(int id);
        Task<ApiResponse<List<ProjectTypeDto>>> GetList();
    }
}
