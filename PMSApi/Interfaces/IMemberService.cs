﻿using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSApi.Interfaces
{
    public interface IMemberService
    {
        Task<ApiResponse<MemberDto>> Create(MemberDto data);
        Task<ApiResponse<MemberDto>> Update(MemberDto data);
        Task<ApiResponse<bool>> Delete(int id);
        Task<ApiResponse<MemberDto>> GetById(int id);
        Task<ApiResponse<List<MemberDto>>> GetList();
    }
}
