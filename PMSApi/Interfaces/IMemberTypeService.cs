﻿using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSApi.Interfaces
{
    public interface IMemberTypeService
    {
        Task<ApiResponse<MemberTypeDto>> Create(MemberTypeDto data);
        Task<ApiResponse<MemberTypeDto>> Update(MemberTypeDto data);
        Task<ApiResponse<bool>> Delete(int id);
        Task<ApiResponse<MemberTypeDto>> GetById(int id);
        Task<ApiResponse<List<MemberTypeDto>>> GetList();
    }
}
