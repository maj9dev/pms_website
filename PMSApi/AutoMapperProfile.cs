﻿using AutoMapper;
using PMSApi.Dtos;
using PMSApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSApi
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // Project
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDto, Project>();

            // Project Member
            CreateMap<ProjectMember, ProjectMemberDto>();
            CreateMap<ProjectMemberDto, ProjectMember>();

            // Project Type
            CreateMap<ProjectType, ProjectTypeDto>();
            CreateMap<ProjectTypeDto, ProjectType>();

            // Project Status
            CreateMap<ProjectStatus, ProjectStatusDto>();
            CreateMap<ProjectStatusDto, ProjectStatus>();

            // Member
            CreateMap<Member, MemberDto>();
            CreateMap<MemberDto, Member>();

            // Member Type
            CreateMap<MemberType, MemberTypeDto>();
            CreateMap<MemberTypeDto, MemberType>();

        }
    }
}
